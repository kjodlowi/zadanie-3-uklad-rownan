#include "Matrix.hh"


using namespace std;

/*
 *  Tutaj nalezy zdefiniowac odpowiednie metody
 *  klasy Matrix, ktore zawieraja wiecej kodu
 *  niz dwie linijki.
 *  Mniejsze metody mozna definiwac w ciele klasy.
 */

std::ostream &operator<<(std::ostream &stream, const Matrix &matrix)
{
    for (int i=0;i < SIZE;i++)
    {
        for (int j = 0; j < SIZE;j++)
        {
            cout << matrix(j,i) << "  ";
        }
        cout << endl;
    }
    return stream;
}

std::istream &operator>>(std::istream &stream, Matrix &matrix)
{
    for (int i=0;i < SIZE;i++)
    {
        for (int j = 0; j < SIZE;j++)
        {
           cin >> matrix(j,i);
        }
    }
    return stream;
}

float determinant(Matrix matrix)
{
    int n = 0, m = 0, j = 1;
    float tmp;

    do 
    {
        if (matrix(n,n) != 0)
        {
            j = 1;

            while (n + j < SIZE)
            {
                tmp = matrix(n+j,n) / matrix(n,n);
                while (m < SIZE)
                {
                    matrix(n+j,m) -= matrix(n,m) * tmp;
                    m++;
                }
                m = 0;
                j++;
            }
            n++;
            j = 1;
            m = 0;
        }else
        {
            while (m < SIZE)
            {
                tmp = matrix(n,m);
                matrix(n,m) = matrix(n+j,m);
                matrix(n+j,m) = tmp;
                m++;
            }
            j++;
            if (n+j >= SIZE)
            {
                if (matrix(n,n) != 0)
                {
                    n++;
                }else
                {
                    tmp = 0;
                    n = SIZE + 1;
                }
                
            }
        }
    }while (n < SIZE);

    tmp = 1;
    n = -1;
    while (n < SIZE -1)
    {
       n++; 
       tmp *= matrix(n,n);
    }

    return tmp;
}

Vector operator*(Matrix matrix, Vector vec)
{
    Vector result;
    float tmp;

    for (int i = 0; i < SIZE; i++)
    {
        tmp = 0;
        for (int j = 0; j < SIZE; j++)
        {
            tmp += matrix(i,j) * vec(j);
        }
        result(i) = tmp;
    }
    return result;
}

Matrix operator*(const Matrix &matrix1, const Matrix &matrix2)
{
    Matrix result;
    float tmp;

    for (int i = 0; i < SIZE; i++)
    {
        for (int j = 0; j < SIZE; j++)
        {
            tmp = 0;
            for (int k = 0; k < SIZE; k++)
            {
                tmp += matrix1(i,k) * matrix2(k,j);
            }
            result(i,j) = tmp;
        }
        
    }
    return result;
}