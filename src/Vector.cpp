#include <iostream>

#include "Size.hh"
#include "Vector.hh"

using namespace std;

/*
 *  Tutaj nalezy zdefiniowac odpowiednie metody
 *  klasy Vector, ktore zawieraja wiecej kodu
 *  niz dwie linijki.
 *  Mniejsze metody mozna definiwac w ciele klasy.
 */

std::istream &operator>>(std::istream &stream, Vector &vec)
{
    for (int i = 0; i < SIZE; i++)
    {
        cin >> vec(i);
    }
    return stream;
}

std::ostream &operator<<(std::ostream &stream, const Vector &vec)
{
    for (int i = 0; i < SIZE; i++)
    {
        cout << vec(i) << " ";
    }
    return stream;
}


Vector operator+(Vector vec1, Vector vec2)
{
    Vector result;

    for (int i = 0; i < SIZE; i++)
    {
        result(i) = vec1(i) + vec2(i);
    }
    return result;
}

Vector operator-(Vector vec1, Vector vec2)
{
    Vector result;

    for (int i = 0; i < SIZE; i++)
    {
        result(i) = vec1(i) - vec2(i);
    }
    return result;
}

float operator*(Vector vec1, Vector vec2)
{
    float result = 0;

    for (int i = 0; i < SIZE; i++)
    {
        result += vec1(i) * vec2(i);
    }
    return result;
}


Vector operator*(Vector vec, float t)
{
    Vector result;
    
    for (int i = 0; i < SIZE; i++)
    {
        result(i) = vec(i) * t;
    } 
    return result;
}

Vector operator/(Vector vec, float t)
{
    Vector result;
    
    for (int i = 0; i < SIZE; i++)
    {
        result(i) = vec(i) / t;
    }
    return result;
}