#include <iostream>
#include "Vector.hh"
#include "Matrix.hh"
#include "Size.hh"
#include "SystemOfLinearEquations.hh"

using namespace std;

/*
 * Tu definiujemy pozostale funkcje.
 * Lepiej jednak stworzyc dodatkowy modul
 * i tam je umiescic. Ten przyklad pokazuje
 * jedynie absolutne minimum.
 */

int main()
{
  SystemOfLinearEquations linear_equation;
  cout << endl
       << "Start programu " << endl
       << endl;
  cin >> linear_equation;

  linear_equation = solve(linear_equation);

  cout << linear_equation;

}
