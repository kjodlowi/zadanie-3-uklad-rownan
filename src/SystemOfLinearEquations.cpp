#include <cmath>
#include <iomanip>

#include "SystemOfLinearEquations.hh"

using namespace std;
/*
 *  Tutaj nalezy zdefiniowac odpowiednie metody
 *  klasy SystemOfLinearEquations, ktore zawieraja 
 *  wiecej kodu niz dwie linijki.
 *  Mniejsze metody mozna definiwac w ciele klasy.
 */

istream &operator>>(istream &stream, SystemOfLinearEquations &system)
{
    cin >> system.matrix >> system.vector;
    
    return stream;
}

ostream &operator<<(ostream &stream, const SystemOfLinearEquations &system)
{
    cout << "Macierz A:" 
        << endl
        << system.matrix
        << endl
        << "Wektor wyrazow wolnych b:" 
        << endl
        << system.vector
        << endl 
        << endl 
        << "Rozwiazanie:" 
        << endl
        << system.result
        << endl
        << endl
        << "Wektor bledu: " << system.err
        << endl
        << "Dlugosc wektora bledu: " << sqrt(abs(system.err * system.err))
        << endl;

    return stream;
}


SystemOfLinearEquations gauss(SystemOfLinearEquations system)
{
    int n = 0, m = 0, j = 1;
    float tmp,vec;

    do 
    {
        if (system.matrix(n,n) != 0)
        {
            j = 1;         

            while (n + j < SIZE)
            {
                tmp = system.matrix(n+j,n) / system.matrix(n,n);
                while (m < SIZE)
                {
                    system.matrix(n+j,m) -= system.matrix(n,m) * tmp;
                    m++;
                }
                system.vector(n+j) -= system.vector(n) * tmp;
                m = 0;
                j++;
            }

            n++;
            j = 1;
            m = 0;
        }else
        {

            vec = system.vector(n);
            system.vector(n) = system.vector(n+j);
            system.vector(n+j) = vec;
            while (m < SIZE)
            {
                tmp = system.matrix(n,m);
                system.matrix(n,m) = system.matrix(n+j,m);
                system.matrix(n+j,m) = tmp;

                m++;
            }
            j++;
            if (n+j >= SIZE)
            {
                n++;
            }
        }
    }while (n < SIZE);

    return system;
}

SystemOfLinearEquations solve(SystemOfLinearEquations system)
{
    SystemOfLinearEquations tmp;  
    int j;

    tmp = gauss(system);
    tmp.result = tmp.vector;

    for (int i = SIZE-1 ; i > -1; i--)
    {
        for (j = SIZE-1; j > i; j--)
        {
            tmp.result(i) -=  tmp.matrix(i,j) * tmp.result(j);
        }
        tmp.result(i) = tmp.result(i)/tmp.matrix(i,j); 
    }
    system.result = tmp.result;

    system.err = system.matrix*system.result - system.vector;

    return system;
}
