#pragma once

#include "Vector.hh"
#include "Size.hh"
#include <iostream>


/*
* Klasa Matrix modeluje wektor składający się z SIZE * SIZE elementow.
* Wektor jest zbudowany na prywatnej tablicy dwuwymiarowej o wymiarach [SIZE][SIZE].
* Dostęp do tablicy _tab[][] odbywa się poprzez zdefiniowane metody
* getVal(int, int) oraz setVal(float, int, int), które pozwalają na sprawdzenie
* zawartości poszegolnych elementowo oraz ich zmiane.
*/
class Matrix
{
  Vector _tab[SIZE];
public:

  /* metoda, która zwraca wartosc przypisana do
  * elementu [n][m] macierzy 
  */

  float operator()(int n, int m) const { return _tab[n](m);  }

  /* metoda, która zamienia wartosc elementu [n][m] macierzy 
  * na wartosc typu float - elem
  */
  float & operator()(int n, int m)  { return _tab[n](m);  }


};

/*! Przeciążenie operatora >>
* Agrumenty:
* - stream - strumień, z którego wczytujemy dane
* - matrix - macierz, do któregj wczytujemy dane
*
* Zapisuje dane w postaci zmiennych typu dobule do tablicy 
* macierzy matrix.
*/
std::istream &operator>>(std::istream &stream, Matrix &matrix);

/*! Przeciążenie operatora <<
* Agrumenty:
* - stream - strumień, na który wypisujemy dane
* - matrix - macierz, na która wypisujemy dane
*
* Wypisuje zmienne z tablicy macierzy matrix na strumień stream. 
*/
std::ostream &operator<<(std::ostream &stream, const Matrix &matrix);

/*! Wyliczenie wyznacznika macierzy.
 * Agrument:
 * - matrix - macierz, której wyznacznik chemy policzyć
 * Zwraca: 
 * Wartość float, która jest wyznacznikiem macierzy matrix.
 */
float determinant(Matrix matrix);

/*! Przeciążenie operatora * 
* Arguemnty:
* - matrix - macierz, którą mnożymy
* - vec - wektor, przez który mnożymy macierz
* Zwraca:
* Wektor, który jest wynikiem mnożenia.
*/
Vector operator*(Matrix matrix, Vector vec);

/*! Przeciążenie operatora * 
* Arguemnty:
* - matrix1 - macierz, którą mnożymy
* - matrix2 - macierz, przez którą mnożymy
* Zwraca:
* Macierz, która jest wynikiem mnożenia.
*/
Matrix operator*(const Matrix &matrix1, const Matrix &matrix2);