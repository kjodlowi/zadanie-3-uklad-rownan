#pragma once
#include <cmath>
#include <iostream>
#include "Matrix.hh"
#include "Vector.hh"
 

/*
 * Klasa SystemOfLinearEuations sklada sie z:
 *  macierzy - matrix - przechowuje wprowadzona w postaci transponowanej macierz
 *  wektora - result - przechowuje na poczatku pusty wektor z rozwiazaniem rownan
 *  wektora - vector - przechowuje wektor wyrazow wolnych b
 *  wektora - err - wektor bledu
 * 
 * Przeciazone sa operatory << i >> pozwalajace zapisywac i wyswietlac uklad rownan liniowych.
 * Klasa posiada metody:
 * - gauss(SystemOfLinearEquations) - zwraca uklad rownan sprowadzony do postaci schodkowej
 * - solve(SystemOfLinearEquations) - zwraca uklad rownan z rozwiazaniem zapisanym w wektorze result
 * - err(SystemOfLinearEquations ) - zwraca uklad rownan z wektorem bledu zapisanym w wektorze err
 */
class SystemOfLinearEquations
{  

public: 
  Matrix matrix;
  Vector result;
  Vector vector;
  Vector err;
};

/*! Przeciazenie operatora >>, wczytujace liniowy uklad rownan
 * Argumenty:
 * - stream - strumien wejsciowy
 * - system - uklad rownan, gdzie zapisujemy odczyt ze strumienia wejscia
 */
std::istream &operator>>(std::istream &stream, SystemOfLinearEquations &system);

/*! Przeciazenie operatora <<tor wyrazów wolnych.  Ponadto program dla znalezionego rozwi  ̨azaniapowinien wyliczy ́c bł  ̨ad wynikaj  ̨acy z niedokładno ́sci oblicze ́n.  Reprezentantem bł ̨edu b ̨edziewektor ró ̇znic przedstawiony poni ̇zeory chcemy wypisac
* Wypisuje uklad rownan na strumieniu wyjsciowym.
*/
std::ostream &operator<<(std::ostream &stream, const SystemOfLinearEquations &system);

/*! Funkcja sprowadzajaca macierz do postaci schodkowej
* Agrumenty:
* - matrix - macierz, ktora chcemy sprowadzic do postaci schodkowej
* Zwraca:
* - macierz w postaci schodkowej;
*/
SystemOfLinearEquations gauss(SystemOfLinearEquations system);

/*! Funkcja rozwiązująca liniowy uklad rownan metoda Gaussa.
* Argumenty:
* - system - uklad rownan, ktory chcemy rozwiazac
* Zwraca: 
* - ten sam uklad rownan, z uzupelnionym wektorem przechowujacym rozwiazanie ukladu - result
*/
SystemOfLinearEquations solve(SystemOfLinearEquations system);
