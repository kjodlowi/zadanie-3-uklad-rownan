#pragma once

#include "Size.hh"
#include <iostream>

using namespace std;


/*
* Klasa Vector modeluje wektor składający się z SIZE elementow.
* Wektor jest zbudowany na prywatnej tablicy o rozmiarze SIZE.
* Dostęp do tablicy _tab[] odbywa się poprzez zdefiniowane metody
* getVal(int) oraz setVal(int, float), które pozwalają na sprawdzenie
* zawartości poszegolnych elementowo oraz ich zmiane.
*/
class Vector
{
  float _tab[SIZE];

public:

  float operator()(int i) const
  {
    return _tab[i];
  }

  float& operator()(int i)
  {
    return _tab[i];
  }

};

/*! Przeciążenie operatora >>
* Agrumenty:
* -stream - strumień, z którego wczytujemy dane
* -vec - wektor, do którego wczytujemy dane
*
* Zapisuje dane w postaci zmiennych typu dobule do tablicy 
* wektora vec.
*/
std::istream &operator>>(std::istream &stream, Vector &vec);

/*! Przeciążenie operatora <<
* Agrumenty:
* -stream - strumień, na który wypisujemy dane
* -vec - wektor, na który wypisujemy dane
*
* Wypisuje zmienne z tablicy wektora vec na strumień stream. 
*/
std::ostream &operator<<(std::ostream &stream, const Vector &vec);


/*! Przeciążenie operatora +
* Agrumenty:
* - vec1, vec2 - wektory, które dodajemy
*
* Zwraca wektor, który jest sumą wektorów vec1 i vec2
*/
Vector operator+(Vector vec1, Vector vec2);

/*! Przeciążenie operatora -
* Agrumenty:
* - vec1, vec2 - wektory, które odejmujemy
*
* Zwraca wektor, który jest sumą wektorów vec1 i -vec2
*/
Vector operator-(Vector vec1, Vector vec2);

/*! Przeciążenie operatora *
* Agrumenty:
* - vec1, vec2 - wektory, które mnożymy
*
* Zwraca liczbę typu float, która jest
* iloczynem skalarnym wektorow vec1, vec2.
*/
float operator*(Vector vec1, Vector vec2);

/*! Przeciążenie operatora *
* Agrumenty:
* - vec1 - wektor który mnożymy przez liczbę
* - t - zmienna typu float, przez którą mnożymy wektor
*
* Zwraca wektor vec1, przemnożony przez liczbe t;
*/
Vector operator*(Vector vec, float t);

/*! Przeciążenie operatora /
* Agrumenty:
* - vec1 - wektor który dzielimy przez liczbę
* - t - zmienna typu float, przez którą dzielimy wektor
*
* Zwraca wektor vec1, podzielony przez liczbe t;
*/
Vector operator/(Vector vec, float t);

